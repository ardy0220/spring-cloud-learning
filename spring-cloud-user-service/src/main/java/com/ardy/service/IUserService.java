package com.ardy.service;

import com.ardy.entry.User;

public interface IUserService {
    User addUser(User user);

    User getUser(String userId);
}
