package com.ardy.service;

//~--- non-JDK imports --------------------------------------------------------

import com.ardy.entry.User;
import com.ardy.repository.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

//~--- classes ----------------------------------------------------------------

/**
 * Class UserServie
 * Description
 * Create 2017-03-17 10:33:08
 * @author Ardy
 */
@Service
@Transactional
public class UserServie implements IUserService {

    /**
     * Field userRepository
     * Description
     */
    @Autowired
    private UserRepository userRepository;

    /**
     * Method addUser
     * Description 说明：
     *
     * @param user 说明：
     *
     * @return 返回值说明：
     */
    @Override
    public User addUser(User user) {
        return userRepository.save(user);
    }

    /**
     * Method getUser
     * Description 说明：
     *
     * @param userId 说明：
     *
     * @return 返回值说明：
     */
    @Override
    public User getUser(String userId) {
        return userRepository.findOne(userId);
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
