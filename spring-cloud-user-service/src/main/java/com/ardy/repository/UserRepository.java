package com.ardy.repository;

import com.ardy.entry.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User,String>{

    @Override
    User save(User entity);

    @Override
    User findOne(String s);

    @Override
    void delete(String s);
}
