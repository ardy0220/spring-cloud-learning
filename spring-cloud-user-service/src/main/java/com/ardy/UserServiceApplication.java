package com.ardy;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Hello world!
 *
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableTransactionManagement
public class UserServiceApplication
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
        new SpringApplicationBuilder(UserServiceApplication.class).run(args);
    }
}
