package com.ardy.controller;


import com.ardy.entry.User;
import com.ardy.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController  {

    @Autowired
    private IUserService userService;

    @RequestMapping(value = "/query",method = RequestMethod.GET)
    public User getUser(){
        return userService.getUser("admin");
    }


    @RequestMapping(value = "/add",method = RequestMethod.POST)
    public User addUser(){
        User user = new User();

        user.setUserId("admin");
        user.setUserName("admin");
        return userService.addUser(user);
    }
}
