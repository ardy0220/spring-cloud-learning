package com.ardy.service;


import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(value = "auth-service",fallback = UserServiceHystrix.class)
public interface UserService {

    @RequestMapping(value = "/query",method = RequestMethod.GET)
    String getAuth();

}
