package com.ardy.service;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class AuthService  {

    private static final Logger LOGGER = LoggerFactory .getLogger(AuthService.class);


    @Autowired
    private RestTemplate restTemplate;


    //@HystrixCommand(fallbackMethod = "getAuthCallback")
    public String getAuth(){
        return restTemplate.getForEntity("http://AUTH-SERVICE/query/", String.class).getBody();
    }




    public String getAuthCallback(){
        return "error";
    }


    public boolean addAuthAndUser(){
        restTemplate.postForEntity("http://USER-SERVICE/add",null,String.class).getBody();
        restTemplate.postForEntity("http://AUTH-SERVICE/add",null,String.class).getBody();
        return true;
    }
}
