package com.ardy.controlller;

import com.ardy.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/hystrix")
public class HystrixClientController {

    @Autowired
    private AuthService authService;


    @RequestMapping("/query")
    public String query(){
        return authService.getAuth();
    }

    @RequestMapping("/add")
    public boolean add(){
        return authService.addAuthAndUser();
    }

}
