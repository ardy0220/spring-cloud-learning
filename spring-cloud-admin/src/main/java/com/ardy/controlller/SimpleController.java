package com.ardy.controlller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/simple")
public class SimpleController {

    @Autowired
    public RestTemplate restTemplate;


    @RequestMapping("/query")
    public String getAuth() {
        return restTemplate.getForEntity("http://AUTH-SERVICE/query", String.class).getBody();
    }

}
