package com.ardy.controlller;

import com.ardy.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/feign")
public class FeignClientController
{
    @Autowired
    private UserService userService;


    @RequestMapping(value = "/query",method = RequestMethod.POST)
    public String getAuth(){
        return userService.getAuth();
    }


}
