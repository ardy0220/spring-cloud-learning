package com.ardy.repository;

import com.ardy.entry.Auth;
import org.springframework.data.repository.CrudRepository;

public interface AuthRepository extends CrudRepository<Auth,String> {

    @Override
    Auth save(Auth entity);

    @Override
    Auth findOne(String s);

    @Override
    void delete(String s);
}
