package com.ardy.entry;


import javax.persistence.*;
import java.sql.Timestamp;

@Entity
public class Auth {


    @Id
    private String userId;

    @Column
    private String passWord;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    @Column
    private Timestamp createTime;
}
