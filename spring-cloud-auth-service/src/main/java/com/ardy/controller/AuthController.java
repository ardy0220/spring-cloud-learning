package com.ardy.controller;

//~--- non-JDK imports --------------------------------------------------------

import com.ardy.entry.Auth;
import com.ardy.service.IAuthService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

//~--- classes ----------------------------------------------------------------

/**
 * Class AuthController
 * Description
 * Create 2017-03-17 10:38:21
 * @author Ardy
 */
@RestController
public class AuthController {

    /**
     * Field authService
     * Description
     */
    @Autowired
    private IAuthService authService;

    /**
     * Method getAuth
     * Description 说明：
     *
     * @return 返回值说明：
     */
    @RequestMapping(
        value  = "/query",
        method = RequestMethod.GET
    )
    public Auth getAuth() {
        return authService.getAuth("admin");
    }

    /**
     * Method addAuth
     * Description 说明：
     *
     * @return 返回值说明：
     */
    @RequestMapping(
        value  = "/add",
        method = RequestMethod.POST
    )
    public Auth addAuth() {
        Auth auth = new Auth();

        auth.setUserId("admin");
        auth.setPassWord("admin");

        return authService.addAuth(auth);
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
