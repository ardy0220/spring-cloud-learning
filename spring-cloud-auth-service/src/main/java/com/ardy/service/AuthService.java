package com.ardy.service;

//~--- non-JDK imports --------------------------------------------------------

import com.ardy.entry.Auth;
import com.ardy.repository.AuthRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

//~--- classes ----------------------------------------------------------------

/**
 * Class AuthService
 * Description
 * Create 2017-03-17 10:33:00
 * @author Ardy
 */
@Service
@Transactional
public class AuthService implements IAuthService {

    /**
     * Field authRepository
     * Description
     */
    @Autowired
    private AuthRepository authRepository;

    /**
     * Method addAuth
     * Description 说明：
     *
     * @param auth 说明：
     *
     * @return 返回值说明：
     */
    @Override
    public Auth addAuth(Auth auth) {
        return authRepository.save(auth);
    }

    /**
     * Method getAuth
     * Description 说明：
     *
     * @param userId 说明：
     *
     * @return 返回值说明：
     */
    @Override
    public Auth getAuth(String userId) {
        return authRepository.findOne(userId);
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
