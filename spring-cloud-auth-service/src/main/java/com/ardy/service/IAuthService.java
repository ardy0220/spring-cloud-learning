package com.ardy.service;

import com.ardy.entry.Auth;

public interface IAuthService {

    Auth addAuth(Auth auth);

    Auth getAuth(String userId);
}
